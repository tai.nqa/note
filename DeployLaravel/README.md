# Deploy Laravel project on CentOS 7

### Server

1. Copy project to **/var/www/**
2. Create **<servername>.conf** in **/etc/httpd/conf.d/**
    ```
    <VirtualHost *:82> 
      ServerName example.com 
      DocumentRoot /var/www/<project-name>/public
      <Directory /var/www/<project-name>/public>
        AllowOverride All
      </Directory>
    </VirtualHost>
    ```
3. Add `Listen 82` (or `Listen 0.0.0.0:82` for ip4) to **/etc/httpd/conf/httpd.conf**

    If you add a new port, you need to add it to httpd port and firewall port
    - Add to httpd port: `sudo semanage port -a -t http_port_t -p tcp 82`
    - Add to firewall: `sudo firewall-cmd --zone=public --permanent --add-port=82/tcp`


4. Run `setsebool httpd_can_network_connect 1` to allow connection to project
5. Run `setsebool httpd_can_network_connect_db 1` to allow connection to DB
6. Restart all service to make it work `service httpd restart`, `sudo firewall-cmd --reload`
7. Go to project folder in **/var/www**
8. Run `chown -R apache:apache storage`, `chmod -R 755 /var/www/<project-name>`, `chmod -R 755 /var/www/<project-name>/storage` to set proper permissions.
### Project

9. Run `cp .env.example .env`
10. Change eviroment variable in **.env** file
11. Run `php artisan key:generate`
12. Run `php artisan migrate`
13. Run `php artisan db:seed`

**Test website at [server-ip]:82**

### Basic authencicate with apache
1. Create login info `htpasswd /etc/httpd/conf/.htpasswd <username>` (use -c option if this is the initial registration)
2. Add to file conf of project in /etc/httpd/conf.d/
```
<Directory [project path]>
    AuthType Basic
    AuthName "Basic Authentication"
    AuthUserFile /etc/httpd/conf/.htpasswd
    require valid-user
</Directory>
```
# Deploy Laravel project on Ubuntu 18.04

### Server

1. Copy project to **/var/www/html/<project-name>**
2. Run `sudo chgrp -R www-data /var/www/html/<project-name>/`, `sudo chmod -R 775 /var/www/html/<project-name>/storage` to set proper permissions.
3. Go to apache site available folder `cd /etc/apache2/sites-available`
4. Create new file config `sudo nano <project-name>.conf` with content
    ```
    <VirtualHost *:81> //or any port you want
       ServerName example.com
       ServerAdmin admin@example.com
       DocumentRoot /var/www/html/<project-name>/public
    
       <Directory /var/www/html/<project-name>>
           AllowOverride All
       </Directory>
       ErrorLog ${APACHE_LOG_DIR}/error.log
       CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>
    ```
5. Enabled site `sudo a2ensite <project-name>`
6. Add port 81 to apache listen `echo "Listen 81" | sudo tee /etc/apache2/ports.conf`
7. Reload apache `sudo systemctl reload apache2`
8. Verify that Apache is now listening on port that you config `sudo netstat -tlpn`

### Project

9. Run `cp .env.example .env`
10. Change eviroment variable in **.env** file
11. Run `php artisan key:generate`
12. Run `php artisan migrate`
13. Run `php artisan db:seed`

**Test website at [server-ip]:81**

If can't access to website through IP, please check ufw or run `sudo ufw allow <port>/tcp` to allow port connection.

# Custom PHP version specify for each website
1. Install PHP version with fpm `sudo apt-get install php7.4 php7.4-fpm php7.4-mysql libapache2-mod-php7.4 libapache2-mod-fcgid -y`
2. Start PHP version FPM: `sudo systemctl start php7.4-fpm`
3. Add this to vitural host (after Directory tag)

    ```
    <FilesMatch \.php$>
          # For Apache version 2.4.10 and above, use SetHandler to run PHP as a fastCGI process server
          SetHandler "proxy:unix:/run/php/php7.4-fpm.sock|fcgi://localhost"
    </FilesMatch>
    ```
4. Restart apache `sudo systemctl reload apache2`

# Basic authencicate with apache
1. Create login info `htpasswd /etc/apache2/.htpasswd <username>` (use -c option if this is the initial registration)
2. Add to Directory tag in vitural host in **/etc/apache2/sites-available/<project-name>**
    ```
    <Directory /var/www/html/<project-name>>
        AuthType Basic
        AuthName "Basic Authentication"
        AuthUserFile /etc/apache2/.htpasswd
        require valid-user
    </Directory>
    ```
3. Restart apache `sudo systemctl reload apache2`

# Change SSH Port
1. Check ssh port currently running `netstat -tulnp | grep ssh`
2. Open `/etc/ssh/sshd_config` and change `#Port 22` to any port like `Port 22000`
3. Restart the SSH server `systemctl restart sshd`
4. Add new rule to Firewall if neccessary `sudo ufw allow 22000`
5. Login to new SSH port via `ssh -p 22000 192.168.1.111`

